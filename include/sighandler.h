/**
 * @date      2022/01/21
 * @auther    luke_guangzhong\@qq.com
 * @file      sighandler.h
 * @Software  CLion
 */

#ifndef SIGHANDLER_H
#define SIGHANDLER_H

#ifndef DEBUG
#define SigHandlerDebug
#else
#define SigHandlerDebug(format, args...) printf("[SH_DEBUG][%s][%d]:"format"\n",__FUNCTION__,__LINE__, ##args);fflush(stdout);
#endif

void InitSigHandler();

void SigHandler(int sig);

#endif //SIGHANDLER_H
