#include <stdio.h>
#include "LinkQueue.h"
#include "sighandler.h"

#ifndef DEBUG
#define MainDebug
#else
#define MainDebug(format, args...) printf("[Main_DEBUG][%s][%d]:"format"\n",__FUNCTION__,__LINE__, ##args);fflush(stdout);
#endif

int main()
{
    printf("Hello, World!\n");

    InitSigHandler();
    QueueID Qid = NULL;
    LinkQueue *queue = getShm_Queue(&Qid, LINKQUEUE_KEY);
    QData data;
    if (Qid == -1 || queue == NULL) {
        queue = createShm_Queue(&Qid, LINKQUEUE_KEY);
        initQueue(queue);
        for (int i = 0; i < 3; ++i) {
            data.data = random_int();
            enQueue(queue, data);
        }
    }
    MainDebug("finish init!");
    printQueue(queue);
    sleep(10);
    deQueue(queue, &data);
    MainDebug("dequeue finished");
    printQueue(queue);
    sleep(10);
    enQueue(queue, data);
    MainDebug("add,again");
    printQueue(queue);
    sleep(10);
    for (int i = 0; i < 3; ++i) {
        data.data = random_int();
        enQueue(queue, data);
    }
    printQueue(queue);
    sleep(10);
    rmQNode(queue, data);
    printQueue(queue);
    sleep(10);
    clearQueue(queue, &Qid);
    MainDebug("clear queue finish");
    printQueue(queue);
    sleep(10);
    return 0;
}
