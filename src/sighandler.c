/**
 * @date      2022/01/21
 * @auther    luke_guangzhong\@qq.com
 * @file      sighandler.c
 * @Software  CLion
 */

#include <signal.h>
#include "sighandler.h"
#include "LinkQueue.h"

void SigHandler(int sig);


void InitSigHandler() {
    SigHandlerDebug("signal triggered!!!");
    signal(SIGINT, SigHandler);
    signal(SIGSEGV, SigHandler);
    signal(SIGTERM,SigHandler);
}


void SigHandler(int sig) {
    QueueID Qid = 0;
    LinkQueue *operator = getShm_Queue(&Qid, LINKQUEUE_KEY);
    switch (sig) {
        case SIGSEGV: {
            SigHandlerDebug("SIGSEGV RECEIVED!");
            break;
        }
        case SIGINT: {
            SigHandlerDebug("SIGINT RECEIVED!");
            break;
        }
        case SIGTERM:{
            SigHandlerDebug("SIGTERM RECEIVED!");
            break;
        }
    }
    if (destroyQueue(&operator, Qid)) {
        SigHandlerDebug("Destory linkqueue failed!!!\n Check the share memory with command: ipc -m!!!");
    } else {
        SigHandlerDebug("Destory linkqueue successed!!!");
    }
    exit(EXIT_FAILURE);
}
